package vovk.kirill.clevertec2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter(val items: Array<Item>, val navController: NavController) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item: Item = items[position]
        val view = holder.itemView
        view.findViewById<ImageView>(R.id.main_img).setBackgroundOnImg()
        view.findViewById<TextView>(R.id.main_title).text = item.title
        view.findViewById<TextView>(R.id.main_desc).text = item.desk
        view.setOnClickListener {
            FirstFragmentDirections.actionFirstFragmentToSecondFragment(position).also {
                navController.navigate(it)
            }
        }
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}