package vovk.kirill.clevertec2

import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import vovk.kirill.clevertec2.databinding.FragmentSecondBinding
import kotlin.system.exitProcess

class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null

    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.mainImg.setBackgroundOnImg()

        binding.btnBack.setOnClickListener {
            val navController =  findNavController()
            SecondFragmentDirections.actionSecondFragmentToFirstFragment().also {
                navController.navigate(it)
            }
        }

         binding.btnExit.setOnClickListener {
             exitProcess(0)
         }

        val id = navArgs<SecondFragmentArgs>().value.id
        val item = Item("Title ${id + 1}", "Description ${id + 1}")
        binding.mainTitle.text = item.title
        binding.mainDesc.text = item.desk
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}