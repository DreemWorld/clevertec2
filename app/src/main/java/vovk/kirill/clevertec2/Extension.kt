package vovk.kirill.clevertec2

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.view.View

fun View.setBackgroundOnImg () {

    val size = resources.getDimensionPixelSize(R.dimen.big_pictures_size)
    val radius = (size / 2).toFloat()
    val bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)
    val paint = Paint()

    paint.color =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            resources.getColor(R.color.background_img, context.theme)
        }
        else {
            resources.getColor(R.color.background_img)
        }
    paint.style = Paint.Style.FILL
    Canvas(bitmap).drawCircle(radius, radius, radius, paint)
    background = BitmapDrawable(resources, bitmap)
}